<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_status')->insert(
        [
        'name' => 'Belum Bayar'
        ],
        );
        DB::table('payment_status')->insert(
        [
        'name' => 'Sudah Bayar'
        ],
        );
    }
}
