<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert(
        [
        'name' => 'webdev',
        'username' => 'webdev',
        'password' => Hash::make('masuk123321'),
        'mobile' => '6285269405552',
        'role' => 1,
        ],
    );
      DB::table('users')->insert(
          [
          'name' => 'admin',
          'username' => 'admin',
          'password' => Hash::make('masuk123'),
          'role' => 1,
          ],
      );
      DB::table('users')->insert(
          [
          'name' => 'operator',
          'username' => 'operator',
          'password' => Hash::make('masuk123'),
          'role' => 2,
          ],
      );

    }
}
