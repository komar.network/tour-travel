<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function showService($id)
    {
        $showService = Destination::where('service_id', $id)->get();
        $services = Service::where('id', $id)->get();

        return view('page.service.show-service', compact('showService', 'services'));
    }
}
