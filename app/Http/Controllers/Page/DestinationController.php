<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use App\Models\Booking;
use Illuminate\Support\Facades\Auth;
use App\Models\Service;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class DestinationController extends Controller
{
    public function detail($id)
    {
        $destinations = Destination::where('id', $id)->first();
        $testimonials = Testimonial::where('destination_id', $id)->get();
        $rating = Testimonial::where('destination_id', $id)->avg('rating');

        return view('page.destination.detail', compact('destinations', 'testimonials', 'rating'));
    }
}
