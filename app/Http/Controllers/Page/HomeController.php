<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Destination;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function landing()
    {
      $services = Service::all();
      $destinations = Destination::all()->take(3);
      return view('page.landing-page', compact('services', 'destinations'));
    }

    public function detail()
    {
      return view('page.detail');
    }
}
