<?php

namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use App\Models\Booking;
use App\Models\Testimonial;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function index()
    {
        $bookings = Booking::where('user_id', Auth::user()->id)->get();
        return view('page.booking.index', compact('bookings'));
    }

    public function store(Request $request)
    {
        // $request->validate([
        //     'user_id' => 'required',
        //     'destination_id' => 'required',
        //     'adult' => 'required',
        //     'child' => 'required',
        //     'check_in' => 'required',
        //     'check_out' => 'required',
        //     'booking_date' => 'required',
        //     'total_price' => 'required',
        //     'payment_status_id' => 'required',
        // ]);

        $bookings = new Booking();

        $bookings->name = Auth::user()->name . date('Y-m-d', time());
        $bookings->user_id = Auth::user()->id;
        $bookings->destination_id = $request->destination_id;
        $bookings->adult = $request->adult;
        $bookings->child = $request->child;
        $bookings->check_in = $request->check_in;
        $bookings->check_out = $request->check_out;
        $bookings->booking_date = date('Y-m-d', time());
        $bookings->total_price = ($request->destination_price * $request->adult) + (($request->destination_price * $request->child)*0.6);
        $bookings->payment_status_id = 1;

        $bookings->save();

        return redirect()->route('landing')->with('success', 'Booking Success');
    }

    public function show($id)
    {
        $bookings = Booking::find($id);
        $rating = Testimonial::where('destination_id', $bookings->destination_id)->avg('rating');

        return view('page.booking.detail', compact('bookings', 'rating'));
    }

    public function Createtestimonial($id)
    {
        $bookings = Booking::find($id);

        return view('page.testimonial.create', compact('bookings'));
    }

    public function Newtestimonial(Request $request)
    {

        $testimonial = new Testimonial();

        $testimonial->message = $request->message;
        $testimonial->rating = $request->rating;
        $testimonial->booking_id = $request->booking_id;
        $testimonial->user_id = Auth::user()->id;
        $testimonial->destination_id = $request->destination_id;

        $testimonial->save();
    }
}
