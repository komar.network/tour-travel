<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Booking;
use App\Models\Destination;
use Illuminate\Http\Request;
use App\Models\PaymentStatus;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\PaymentDetails;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $booking = Booking::all();
        $destinations = Destination::all();
        $payment_status = PaymentStatus::all();
        $users = User::all();
        return view('admin.booking.index', compact('booking', 'destinations', 'payment_status', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $booking = Booking::findOrFail($id);

        return view('admin.booking.payment', compact('booking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $booking = Booking::findOrFail($id);

        $request->validate([
          'pay'   => 'required',
          'date'  => 'required',
          'type'  => 'required',
        ]);

        if ($booking->status == 1) {
          DB::transaction(function () use($booking, $request) {
              $paymentDetail = PaymentDetails::where('booking_id', $request->id)->get();
              $paid = $paymentDetail->sum('pay');
              $amount = $booking->total_price - ($paid + $request->pay);

              $detail = new PaymentDetails;
              $detail->booking_id = $booking->id;
              $detail->date = $request->date;
              $detail->bill = $booking->total_price;
              $detail->pay = $request->pay;
              $detail->amount = $amount;
              $detail->type = $request->type;
              $detail->added_by = auth()->user()->id;
              $detail->save();

          });
          toast('Data Berhasil Disimpan!', 'success');
          return redirect()->route('booking.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
