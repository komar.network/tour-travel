<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use App\Models\Service;
use Illuminate\Http\Request;
use File;

class DestinationController extends Controller
{
    // Auth Middleware
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $destinations = Destination::join('services', 'destinations.id');
        $services = Service::all();
        $destinations = Destination::all();
        return view('admin.destination.index', compact('destinations', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::all();
        $destinations = Destination::all();
        return view('admin.destination.create', compact('destinations', 'services'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required',
            'region'      => 'required',
            'description' => 'required',
            'image'       => 'required',
            'price'       => 'required',
            'service_id'  => 'required',
        ]);

        $newNameImage = time().'.'.$request->image->extension();
        $request->image->move(public_path('image'), $newNameImage);

        $destinations = new Destination;

        $destinations->name         = $request->name;
        $destinations->service_id   = $request->service_id;
        $destinations->region       = $request->region;
        $destinations->description  = $request->description;
        $destinations->image        = $newNameImage;
        $destinations->price        = $request->price;
        $destinations->save();

        toast('Data Berhasil Disimpan!', 'success');
        return redirect()->route('destination.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $destinations = Destination::where('id', $id)->first();
        $services = Service::all();
        // $defaultService = Service::where('id', $destinations->service_id)->first();

        return view('admin.destination.edit', compact('destinations', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required',
            'region'      => 'required',
            'description' => 'required',
            // 'image'       => 'required',
            'price'       => 'required',
            'service_id'  => 'required',
        ]);

        $destinations = Destination::find($id);

        $destinations->name         = $request->name;
        $destinations->service_id   = $request->service_id;
        $destinations->region       = $request->region;
        $destinations->description  = $request->description;
        $destinations->price        = $request->price;
        if ($request->has('image'))
        {
            $path = 'image/';
            File::delete($path. $destinations->image);

            $newNameImage = time().'.'.$request->image->extension();
            $request->image->move(public_path('image'), $newNameImage);

            $destinations->image        = $newNameImage;
        }

        $destinations->save();

        toast('Data Berhasil Diperbarui!', 'success');
        return redirect()->route('destination.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destinations = Destination::where('id', $id)->delete();

        toast('Data Berhasil Dihapus!', 'success');
        return redirect()->route('destination.index');
    }
}
