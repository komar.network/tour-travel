<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentDetails extends Model
{
    use HasFactory;

    protected $table = 'payment_details';

    protected $guarded = [];

    public function booking()
    {
      return $this->belongsTo(Booking::class, 'booking_id');
    }

    public function addedBy()
    {
      return $this->belongsTo(User::class, 'added_by');
    }
}
