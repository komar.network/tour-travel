<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    use HasFactory;

    protected $table = 'payment_status';

    protected $fillable = [
        'name',
    ];

    public function booking()
    {
        return $this->hasMany(Booking::class, 'payment_status_id', 'id');
    }
}
