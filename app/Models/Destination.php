<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;

    protected $table = 'destinations';

    protected $fillable = [
      'name',
      'region',
      'description',
      'image',
      'price',
      'service_id',
    ];

    public function service()
    {
      return $this->belongsTo(Service::class);
    }

    public function booking()
    {
      return $this->hasMany(Booking::class);
    }

    public function testimonial()
    {
      return $this->hasMany(Testimonial::class);
    }
}
