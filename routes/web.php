<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\DestinationController;
use App\Http\Controllers\Admin\UserAdminController;
use App\Http\Controllers\Admin\BookingController;
use App\Http\Controllers\Admin\PaymentStatusController;
use App\Http\Controllers\Page\HomeController;
use App\Http\Controllers\Page\ServiceController as PageServiceController;
use App\Http\Controllers\Page\DestinationController as PageDestinationController;
use App\Http\Controllers\Page\BookingController as PageBookingController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('template', function () {
  return view('layouts.admin.app');
});


Route::prefix('admin-dashboard')->group(function () {
  Route::get('/', [AdminController::class, 'dashboard'])->name('admin.dashboard');
  Route::resource('service', ServiceController::class)->middleware(['role:1,2']);
  Route::resource('destination', DestinationController::class)->middleware(['role:1,2']);
  Route::resource('booking', BookingController::class)->middleware(['role:1,2']);
  Route::resource('payment', PaymentStatusController::class)->middleware(['role:1,2']);
});

Route::middleware(['role:3'])->group(function () {
});

// Client
Route::get('/', [HomeController::class, 'landing'])->name('landing'); // Landing

Route::get('/destination/{id}', [PageDestinationController::class, 'detail'])->name('destinations'); // Detail Destination

Route::get('/service/{id}', [PageServiceController::class, 'showService'])->name('showService'); // Show Service

Route::post('/booking', [PageBookingController::class, 'store'])->name('newBooking'); // Booking

Route::get('/booking', [PageBookingController::class, 'index'])->name('bookingAll'); // Booking

Route::get('/booking/{id}', [PageBookingController::class, 'show'])->name('bookingShow'); // Booking

Route::get('/testimonial/{id}', [PageBookingController::class, 'Createtestimonial'])->name('createTestimonial'); // Booking

Route::post('/', [PageBookingController::class, 'Newtestimonial'])->name('newTestimonial'); // Booking