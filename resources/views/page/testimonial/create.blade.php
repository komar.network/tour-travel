@extends('layouts.page.app')

@section('content')
<section class="" style="padding-top: 7rem">

  <div class="container">
    <div class="position-absolute start-100 bottom-0 translate-middle-x d-none d-xl-block ms-xl-n4">
      <img src="{{ asset('assets/img/dest/shape.svg') }}" alt="destination" /></div>
    <div class="mb-7 text-center">
      <h5 class="text-secondary">Bookings </h5>
      <h3 class="fs-xl-10 fs-lg-8 fs-7 fw-bold font-cursive text-capitalize"></h3>
    </div>
    <div class="row">
      <form class="container bg-warning rounded-3 py-4" method="POST" action="{{ route('newTestimonial') }}">
            @csrf
            <h1 class="mb-3">Testimonial</h1>
            <div class="form-group">
                <h5>Rating 1-5</h3>
                <input type="number" class="form-control" id="rating" name="rating" placeholder="Rate" min="0" max="5">
                {{-- <small id="dateHelp" class="form-text text-muted">Pilih Tanggal Keberangkatan</small> --}}
            </div>
            <div class="form-group mt-3">
                <h5>Message</h5>
                <textarea type="text" class="form-control " placeholder="Message"
                    value="{{ old('message') }}" name="message" id="message" required></textarea>
                {{-- <small id="dateHelp" class="form-text text-muted">Pilih Tanggal Keberangkatan</small> --}}
            </div>
            <input type="hidden" name="booking_id" value="{{$bookings->id}}">
            <input type="hidden" name="destination_id" value="{{$bookings->destination_id}}">
            <button type="submit" class="btn btn-info mt-4">Submit</button>
        </form>
    </div>
  </div><!-- end of .container-->

  </section>

@endsection
