@extends('layouts.page.app')

@push('styles')

<script src="https://kit.fontawesome.com/f914bc7ad2.js" crossorigin="anonymous"></script>
<style>
    .content_text {
        display: -webkit-box;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .btn_form {
        background-color: black;
    }
    .readMore {
        text-decoration: underline;
    }
    .readMore:hover {
        cursor: pointer;
    }

</style>

@endpush


@push('scripts')

<script>
    function toggleEllipsis() 
    {
        var element = document.getElementById("text_body");
        var btn = document.getElementById("btn_ellipsis");
        
        element.classList.toggle("content_text");

        if (btn.innerHTML === "Read More") {
            btn.innerHTML = "Read Less";
        } else {
            btn.innerHTML = "Read More";
        }
    }

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
    dd = '0' + dd;
    }

    if (mm < 10) {
    mm = '0' + mm;
    } 
        
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("check_out").setAttribute("min", today);
    document.getElementById("check_in").setAttribute("min", today);
</script>

@endpush


@section('content')

@php
    $name = $destinations->name;
@endphp
<section style="padding-top: 7rem;">
    <div class="bg-holder" style="background-image:url(assets/img/hero/hero-bg.svg);">
    </div>
<div class="container">
    <div class="row">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);"
            aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/" class="breadcrumb-home p-2">Home</a></li>
                <li class="breadcrumb-item"><a href="/service/{{$destinations->service->id}}" class="p-2">{{$destinations->service->name}}</a></li>
                <li class="breadcrumb-item active" aria-current="page" style="color:coral">{{ $destinations->name }}, {{ $destinations->region }}</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-7">
            <img src="{{asset('/image/'.$destinations->image)}}" width="100%">
            <div class="title row">
                <h1 class="mt-3">
                    {{ $destinations->name }}, {{ $destinations->region }}
                </h1>
            </div>
            <div class="rating row">
                <h5> <i class="fa-solid fa-star" style="color:orange"></i>  {{round($rating, 1)}}/5</h5>
            </div>  
            <div class="content row">
                <h5>Rp {{ number_format($destinations->price) }}</h5>
                <p class="content_text" id="text_body">
                    {{ $destinations->description }}
                </p>
                <a class="readMore" onclick="toggleEllipsis()" id="btn_ellipsis">Read More</a>
                {{-- <button class="btn btn-primary" onclick="toggleEllipsis()">Read More</button> --}}
            </div>
        </div>
        <div class="col">
            <form class="container bg-warning rounded-3 py-4" method="POST" action="{{ route('newBooking') }}">
                @csrf
                <h1 class="mb-3">Booking</h1>
                <div class="form-group">
                    <h5>Tanggal Berangkat</h3>
                    <input type="date" class="form-control" id="check_in" name="check_in" placeholder="Tanggal Berangkat">
                    {{-- <small id="dateHelp" class="form-text text-muted">Pilih Tanggal Keberangkatan</small> --}}
                </div>
                <div class="form-group mt-3">
                    <h5>Tanggal Pulang</h5>
                    <input type="date" class="form-control" id="check_out" name="check_out" placeholder="Tanggal Pulang">
                    {{-- <small id="dateHelp" class="form-text text-muted">Pilih Tanggal Keberangkatan</small> --}}
                </div>
                <div class="form-group mt-3">
                    <h5>Jumlah Peserta</h5>
                    <div class="row">
                        <div class="col">
                            <label for="adult">Dewasa</label>
                            <input type="number" class="form-control" id="adult" name="adult" placeholder="Dewasa" min="0">
                        </div>
                        <div class="col">
                            <label for="child">Anak-anak</label>
                            <input type="number" class="form-control" id="child" name="child" placeholder="Anak-anak" min="0">
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="destination_id" name="destination_id" value="{{$destinations->id}}">
                    <input type="hidden" class="form-control" id="destination_price" name="destination_price" min="0" value="{{$destinations->price}}">
                </div>
                <button type="submit" class="btn btn-info mt-4">Book</button>
            </form>
        </div>
    </div>
    <div class="row mt-6 overflow auto">
        <h2>Testimonial</h2>
        @foreach ($testimonials as $key => $item)
        @php
            $name = $item->name;
        @endphp
            <div class="card my-2">
            <h5 class="card-header">{{$item->user->name}}</h5>
            <div class="card-body">
                <h5 class="card-title"><i class="fa-solid fa-star" style="color:orange"></i>  {{$item->rating}}/5</h5>
                <p class="card-text">{{$item->message}}</p>
                {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
            </div>
        </div>
        @endforeach
        
    </div>
</div>  
</div>
</section>
    
@endsection