@extends('layouts.page.app')

@push('styles')

<script src="https://kit.fontawesome.com/f914bc7ad2.js" crossorigin="anonymous"></script>
<style>
    .content_text {
        display: -webkit-box;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    .btn_form {
        background-color: black;
    }
    .readMore {
        text-decoration: underline;
    }
    .readMore:hover {
        cursor: pointer;
    }

</style>

@endpush


@push('scripts')

<script>
    function toggleEllipsis() 
    {
        var element = document.getElementById("text_body");
        var btn = document.getElementById("btn_ellipsis");
        
        element.classList.toggle("content_text");

        if (btn.innerHTML === "Read More") {
            btn.innerHTML = "Read Less";
        } else {
            btn.innerHTML = "Read More";
        }
    }

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
    dd = '0' + dd;
    }

    if (mm < 10) {
    mm = '0' + mm;
    } 
        
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("check_out").setAttribute("min", today);
    document.getElementById("check_in").setAttribute("min", today);
</script>

@endpush


@section('content')

@php
    $name = $bookings->name;
@endphp
<section style="padding-top: 7rem;">
    <div class="bg-holder" style="background-image:url(assets/img/hero/hero-bg.svg);">
    </div>
<div class="container">
    <div class="row">
        <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);"
            aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/" class="breadcrumb-home p-2">Home</a></li>
                <li class="breadcrumb-item"><a href="/booking" class="p-2">Booking</a></li>
                <li class="breadcrumb-item active" aria-current="page" style="color:coral">{{ $bookings->destination->name }}, {{ $bookings->destination->region }}</li>
            </ol>
        </nav>
    </div>
    <div class="row">
        <div class="col-7">
            <img src="{{asset('/image/'.$bookings->destination->image)}}" width="100%">
            <div class="title row">
                <h1 class="mt-3">
                    {{ $bookings->destination->name }}, {{ $bookings->destination->region }}
                </h1>
            </div>
            <div class="rating row">
                <h5> <i class="fa-solid fa-star" style="color:orange"></i>  {{round($rating, 1)}}/5</h5>
            </div>  
            <div class="content row">
                <h5>Rp {{ number_format($bookings->total_price) }}</h5>
                <p class="content_text" id="text_body">
                    {{ $bookings->destination->description }}
                </p>
                <a class="readMore" onclick="toggleEllipsis()" id="btn_ellipsis">Read More</a>
                {{-- <button class="btn btn-primary" onclick="toggleEllipsis()">Read More</button> --}}
            </div>

            <h5 class="mt-3">Dewasa: {{$bookings->adult}}</h5>
            <h5>Anak: {{$bookings->child}}</h5>

            <h5 class="mt-3">Tanggal Keberangkatan: {{$bookings->check_in}}</h5>
            <h5>Tanggal Kepulangan: {{$bookings->check_out}}</h5>

            <h3 class="mt-3">Status: {{$bookings->payment_status->name}}</h3>

            <a class="btn btn-success" href="/testimonial/{{$bookings->id}}">
                <i class="bx bx-edit-alt"></i>
                Testimonial
            </a>
        </div>
    </div>
</div>  
</div>
</section>
    
@endsection