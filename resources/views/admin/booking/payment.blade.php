@extends('layouts.admin.app')
@section('title')
  Payment
@endsection

@section('content')
<div class="col-md-7 mx-auto">
  <div class="card mb-4">
      <div class="card-header d-flex justify-content-between align-items-center">
          <div class="">
            <h5 class="card-title m-0">Payment Details</h5>
          </div>
          <div class="">
            <a href="{{ route('booking.index') }}" class="badge badge-pill bg-label-secondary p-2">
                <i class="bx bx-x"></i>
            </a>
          </div>
      </div>

      <div class="card-body">

        <form action="{{ route('booking.update', $booking->id) }}" method="POST">
          @csrf
          @method('put')
          @php
            $detail = App\Models\PaymentDetails::where('booking_id', $booking->id)->latest()->first();
            $tagihan = $detail ? $detail->amount : $booking->total_price;
          @endphp

            <div class="row">
              <div class="col-md-12">

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Nama Pelanggan
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror"
                  placeholder="Nama Services" value="{{ $booking->name }}" name="name" id="name" disabled>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Jumlah Tagihan (Rp)
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control"
                  placeholder="Jumlah" value="{{ number_format($tagihan,0," ,",".") }}" disabled>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Jumlah Bayar
                    <span class="text-danger">*</span>
                  </label>
                  <input type="number" name="pay" min="10000" class="form-control"
                  placeholder="Jumlah" value="{{ $tagihan }}" required>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Tanggal Bayar
                    <span class="text-danger">*</span>
                  </label>
                  <input type="date" name="date" class="form-control" required>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Jenis Pembayaran
                    <span class="text-danger">*</span>
                  </label>
                  <select name="type" id="" class="form-control" required>
                    <option value="1">Transfer</option>
                    <option value="2">Cash</option>
                  </select>
                </div>

            <button type="submit" class="btn btn-primary mt-3 px-5">Submit</button>
          </form>
      </div>
  </div>
</div>
</div>
@endsection
