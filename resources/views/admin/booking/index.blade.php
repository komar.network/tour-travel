@extends('layouts.admin.app')
@section('title')
  List Bookings
@endsection

@push('scripts')
<script>
  $(function () {
    $("#bookingTable").DataTable();
  });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.js"></script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.css"/>
@endpush

@section('content')
  <div class="row">
    <div class="col-md-12">

      <div class="card mb-4">

        <div class="card-header border-bottom d-flex justify-content-between">
          <h5 class="card-title mb-0">@yield('title')</h5>
        </div>

        <div class="row mx-1 my-3">
          <div class="col-12 col-md-6 d-flex align-items-center justify-content-between justify-content-md-start gap-2 mb-3 mb-md-0">
            <div class="text-xl-end text-lg-start text-md-end text-start mt-md-0">
              <a href="{{ route('booking.create') }}" class="btn btn-primary" type="button">
                  <span>
                      <i class="bx bx-plus me-md-2"></i>
                      <span class="d-md-inline-block d-none">Add New Booking</span>
                  </span>
              </a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive text-nowrap">
            <table id="bookingTable" class="stripe">
              <thead class="table-primary">
                  <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Destination</th>
                      <th>Adult</th>
                      <th>Child</th>
                      <th>Check In</th>
                      <th>Check Out</th>
                      <th>Booking Date</th>
                      <th>Price</th>
                      <th>Payment Status</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody class="table-border-bottom-0">

                @forelse($booking as $key => $item)
                @php
                  $name = $item->name;
                @endphp
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $item->user->name }}</td>
                    <td>{{ $item->destination->name }}</td>
                    <td>{{ $item->adult }}</td>
                    <td>{{ $item->child }}</td>
                    <td>{{ $item->check_in }}</td>
                    <td>{{ $item->check_out }}</td>
                    <td>{{ $item->booking_date }}</td>
                    <td>{{ $item->total_price }}</td>
                    <td>{{ $item->payment_status->name }}</td>
                    <td class="col-2">
                      <a class="btn btn-link btn-sm text-light" href="{{ route('booking.edit', $item->id) }}">
                      <i class="bx bx-money "></i>
                        Payment
                      </a>
                    </div>
                  </td>
                </tr>
                @empty

                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
