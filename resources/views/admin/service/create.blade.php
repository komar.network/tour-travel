@extends('layouts.admin.app')
@section('title')
  Add Services | Dashboard
@endsection

@section('content')
<div class="col-md-7 mx-auto">
  <div class="card mb-4">
      <div class="card-header d-flex justify-content-between align-items-center">
          <div class="">
            <h5 class="card-title m-0">Add Services</h5>
          </div>
          <div class="">
            <a href="{{ route('service.index') }}" class="badge badge-pill bg-label-secondary p-2">
                <i class="bx bx-x"></i>
            </a>
          </div>
      </div>

      <div class="card-body">

        <form action="{{ route('service.store') }}" method="POST">
          @csrf

            <div class="row">
              <div class="col-md-12">

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Nama Service
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror"
                  placeholder="Nama Services" value="{{ old('name') }}" name="name" id="name" required="">

                  @error('name')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="description">Descriptions
                        <span class="text-danger">*</span>
                    </label>
                    <textarea type="text" class="form-control " placeholder="Descriptions"
                    value="{{ old('description') }}" name="description" id="description" required></textarea>
                </div>

            <button type="submit" class="btn btn-primary mt-3 px-5">Submit</button>
          </form>
      </div>
  </div>
</div>
</div>
@endsection
