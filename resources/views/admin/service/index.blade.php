@extends('layouts.admin.app')
@section('title')
  List Services
@endsection

@push('scripts')
<script>
  $(function () {
    $("#serviceTable").DataTable();
  });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.js"></script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.css"/>
@endpush

@section('content')
  <div class="row">
    <div class="col-md-12">

      <div class="card mb-4">

        <div class="card-header border-bottom d-flex justify-content-between">
          <h5 class="card-title mb-0">@yield('title')</h5>
        </div>

        <div class="row mx-1 my-3">
          <div class="col-12 col-md-6 d-flex align-items-center justify-content-between justify-content-md-start gap-2 mb-3 mb-md-0">
            <div class="text-xl-end text-lg-start text-md-end text-start mt-md-0">
              <a href="{{ route('service.create') }}" class="btn btn-primary" type="button">
                  <span>
                      <i class="bx bx-plus me-md-2"></i>
                      <span class="d-md-inline-block d-none">Add New Service</span>
                  </span>
              </a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive text-nowrap">
            <table id="serviceTable" class="stripe">
              <thead class="table-primary">
                  <tr>
                      <th>#</th>
                      <th>Name Service</th>
                      <th>Descriptions</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody class="table-border-bottom-0">

                @forelse($services as $key => $item)
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $item->name }}</td>
                    <td >{{ $item->description }}</td>
                    <td class="col-2">
                      <a class="btn btn-link btn-sm text-light" href="{{ route('service.edit', $item->id) }}">
                        <i class="bx bx-edit-alt"></i>
                        Edit
                      </a>
                      <a class="btn btn-link btn-sm text-light" onclick="" data-bs-toggle="modal"
                      data-bs-target="#modal-delete-3_{{ $item->id }}" data-action="{{ route('service.destroy', $item->id) }}">
                      <i class="bx bx-trash "></i>
                          Delete
                      </a>
                      <!-- modal delete-->
                      <div class="modal fade" id="modal-delete-3_{{ $item->id }}" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content">
                          <div class="modal-header">
                              <h5 class="modal-title" id="modal-deleteTitle">Hapus Data</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>

                          <form action="{{ route('service.destroy', $item->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <div class="modal-body">
                                <h5 class="text-center">Yakin, akan menghapus <b>{{ $item->name }}</b> ?</h5>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger">Ya! Hapus</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                @empty

                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
