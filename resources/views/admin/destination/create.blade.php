@extends('layouts.admin.app')
@section('title')
  Add Destination | Dashboard
@endsection

@section('content')
<div class="col-md-7 mx-auto">
  <div class="card mb-4">
      <div class="card-header d-flex justify-content-between align-items-center">
          <div class="">
            <h5 class="card-title m-0">Add Destination</h5>
          </div>
          <div class="">
            <a href="{{ route('destination.index') }}" class="badge badge-pill bg-label-secondary p-2">
                <i class="bx bx-x"></i>
            </a>
          </div>
      </div>

      <div class="card-body">

        <form action="{{ route('destination.store') }}" method="POST"
          enctype="multipart/form-data">
          @csrf

            <div class="row">
              <div class="col-md-12">

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Nama Destination
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror"
                  placeholder="Nama Destinations" value="{{ old('name') }}" name="name" id="name" required="">

                  @error('name')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                  <label class="form-label" for="service_id">
                    Service
                    <span class="text-danger">*</span>
                  </label>
                  <select name="service_id" class="form-control" required>
                    @foreach($services as $item)
                      <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                  </select>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="region">
                    Region
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('region') is-invalid @enderror"
                  placeholder="Nama Region" value="{{ old('region') }}" name="region" id="region" required="">

                  @error('region')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="description">Descriptions
                        <span class="text-danger">*</span>
                    </label>
                    <textarea type="text" class="form-control " placeholder="Descriptions"
                    value="{{ old('description') }}" name="description" id="description" required></textarea>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="image">
                    Image
                    <span class="text-danger">*</span>
                  </label>
                  <input type="file" class="form-control @error('image') is-invalid @enderror"
                  placeholder="Select Images" value="{{ old('image') }}" name="image" id="image" required="">

                  @error('image')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                  <label class="form-label" for="price">
                    Price
                    <span class="text-danger">*</span>
                  </label>
                  <input type="number" class="form-control @error('price') is-invalid @enderror"
                  placeholder="Price Destination" value="{{ old('price') }}" name="price" id="price" required="">

                  @error('price')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

            <button type="submit" class="btn btn-primary mt-3 px-5">Submit</button>
          </form>
      </div>
  </div>
</div>
</div>
@endsection
