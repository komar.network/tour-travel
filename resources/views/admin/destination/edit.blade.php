@extends('layouts.admin.app')
@section('title')
  Edit Destination | Dashboard
@endsection

@section('content')
<div class="col-md-7 mx-auto">
  <div class="card mb-4">
      <div class="card-header d-flex justify-content-between align-items-center">
          <div class="">
            <h5 class="card-title m-0">Edit Destination</h5>
          </div>
          <div class="">
            <a href="{{ route('destination.index') }}" class="badge badge-pill bg-label-secondary p-2">
                <i class="bx bx-x"></i>
            </a>
          </div>
      </div>

      <div class="card-body">

        <form action="{{ route('destination.update', $destinations->id) }}" method="POST"
          enctype="multipart/form-data">
          @csrf
          @method('put')

            <div class="row">
              <div class="col-md-12">

                <div class="mb-3">
                  <label class="form-label" for="name">
                    Nama Destination
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror"
                  placeholder="Nama Destinations" value="{{ $destinations->name }}" name="name" id="name" required="">

                  @error('name')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                  <label class="form-label" for="service_id">
                    Service
                    <span class="text-danger">*</span>
                  </label>
                  <select name="service_id" class="form-control" required selected="">
                    {{-- <option value="{{ $destinations->service_id }}">{{ $item->service->name }}</option> --}}
                    @foreach($services as $item)
                      @if ($item->id === $destinations->service_id)
                          <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                      @else
                          <option value="{{ $item->id }}">{{ $item->name }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="region">
                    Region
                    <span class="text-danger">*</span>
                  </label>
                  <input type="text" class="form-control @error('region') is-invalid @enderror"
                  placeholder="Nama Region" value="{{ $destinations->region }}" name="region" id="region" required="">

                  @error('region')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="description">Descriptions
                        <span class="text-danger">*</span>
                    </label>
                    <textarea type="text" class="form-control " placeholder="Descriptions"
                    value="{{ old('description') }}" name="description" id="description" required>{{ $destinations->description }}</textarea>
                </div>

                <div class="mb-3">
                  <label class="form-label" for="image">
                    Image
                    <span class="text-danger">*</span>
                  </label>
                  <input type="file" class="form-control"
                  placeholder="Select Images" value="{{ $destinations->image }}" name="image" id="image">
                </div>

                <div class="mb-3">
                  <label class="form-label" for="price">
                    Price
                    <span class="text-danger">*</span>
                  </label>
                  <input type="number" class="form-control @error('price') is-invalid @enderror"
                  placeholder="Price Destination" value="{{ $destinations->price }}" name="price" id="price" required="">

                  @error('price')
                    <div class="alert alert-danger mt-2">
                      {{ $message }}
                    </div>
                  @enderror
                </div>

            <button type="submit" class="btn btn-primary mt-3 px-5">Update</button>
          </form>
      </div>
  </div>
</div>
</div>
@endsection
