@extends('layouts.admin.app')
@section('title')
  List Destinations
@endsection

@push('scripts')
<script>
  $(function () {
    $("#destinationTable").DataTable();
  });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.js"></script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.13.1/datatables.min.css"/>
@endpush

@section('content')
  <div class="row">
    <div class="col-md-12">

      <div class="card mb-4">

        <div class="card-header border-bottom d-flex justify-content-between">
          <h5 class="card-title mb-0">@yield('title')</h5>
        </div>

        <div class="row mx-1 my-3">
          <div class="col-12 col-md-6 d-flex align-items-center justify-content-between justify-content-md-start gap-2 mb-3 mb-md-0">
            <div class="text-xl-end text-lg-start text-md-end text-start mt-md-0">
              <a href="{{ route('destination.create') }}" class="btn btn-primary" type="button">
                  <span>
                      <i class="bx bx-plus me-md-2"></i>
                      <span class="d-md-inline-block d-none">Add New Destination</span>
                  </span>
              </a>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="table-responsive text-nowrap">
            <table id="destinationTable" class="stripe">
              <thead class="table-primary">
                  <tr>
                      <th>#</th>
                      <th>Name Destination</th>
                      <th>Service</th>
                      <th>Region</th>
                      <th>Description</th>
                      <th>Image</th>
                      <th>Price</th>
                      <th>Actions</th>
                  </tr>
              </thead>
              <tbody class="table-border-bottom-0">

                @forelse($destinations as $key => $item)
                @php
                  $name = $item->name;
                @endphp
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->service->name }}</td>
                    <td>{{ $item->region }}</td>
                    <td class="text-truncate" style="max-width: 200px;">{{ $item->description }}</td>
                    <td>
                      <div class="d-flex align-content-center align-items-center my-3 overflow-hidden rounded"
                          style="height: 50px; width: 50px;">
                          <img class="mr-3 img-fluid" src="{{asset('/image/'.$item->image)}}" width="50px">
                      </div>
                    </td>
                    <td>{{ $item->price }}</td>
                    <td class="col-2">
                      <a class="btn btn-link btn-sm text-light" href="{{ route('destination.edit', $item->id) }}">
                        <i class="bx bx-edit-alt"></i>
                        Edit
                      </a>
                      <a class="btn btn-link btn-sm text-light" onclick="" data-bs-toggle="modal"
                      data-bs-target="#modal-delete-3_{{ $item->id }}" data-action="{{ route('destination.destroy', $item->id) }}">
                      <i class="bx bx-trash "></i>
                          Delete
                      </a>
                      <!-- modal delete-->
                      <div class="modal fade" id="modal-delete-3_{{ $item->id }}" tabindex="-1" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content">
                          <div class="modal-header">
                              <h5 class="modal-title" id="modal-deleteTitle">Hapus Data</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>

                          <form action="{{ route('destination.destroy', $item->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <div class="modal-body">
                                <h5 class="text-center">Yakin, akan menghapus <b>{{ $item->name }}</b> ?</h5>
                            </div>
                            <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-danger">Ya! Hapus</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </td>
                </tr>
                @empty

                @endforelse
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
